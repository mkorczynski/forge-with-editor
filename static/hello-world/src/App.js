import React, { useEffect, useState } from 'react';
import {Editor, EditorContext, WithEditorActions} from '@atlaskit/editor-core'
import { view, Modal } from '@forge/bridge';
import {
  getEmojiProvider,
  currentUser,
} from "@atlaskit/util-data-test/getEmojiProvider";
import styled from 'styled-components';

export const providers = {
  emojiProvider: getEmojiProvider({
      currentUser,
  }),
};

const Container = styled.div`
  background-color: #eee;
`;

function App() {
  const [context, setContext] = useState();

  useEffect(() => {
    view.getContext().then(value => setContext(JSON.parse(JSON.stringify(value))));
  }, [context]);

  const openModal = () => {
    const modal = new Modal({
      resource: 'main',
      onClose: (payload) => {
          console.log('onClose called with', payload);
      },
      context: {
          customText: "modal"
      },
      size: 'x-large'
    });
    modal.open();
  }

  return (
    <Container>
      {
        context && context?.extension?.modal?.customText 
          ? <p>Modal custom text: {context?.extension?.modal?.customText}</p> 
          : <button onClick={openModal}>Open Modals</button> 
      } 
      <EditorContext>
        <WithEditorActions
            render={(actions) => {
                return (
                    <Editor allowRule={true}
                            allowTextColor={true}
                            allowTables={{
                                allowControls: true,
                            }}
                            allowPanel={true}
                            allowHelpDialog={true}
                            {...providers}
                    />);
            }}
        />
    </EditorContext>
    </Container>
  );
}

export default App;
